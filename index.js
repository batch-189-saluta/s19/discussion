// alert('Hello World')

/*Selection Control Structures
	- sorts out wether the statements are to be executed based on the condition whether it is true or false

	if .. else statement

	Syntax:
		if(condition) {
			//statement
		}
		else {
			//statement
		}


	if statement - executes a statement if a specified condition is true

	Syntax:
		if(condition) {
			//statement
		}

*/

let numA = -1;
if (numA < 0) {
	console.log('Hello')
}
console.log(numA<0)

let city = 'New York'
if (city === 'New York') {
	console.log('Welcome to New York city')
}


/*
	Else if 
		executes a statement if our previous conditions are falls and if the specified condition is true
		-the "else if" clause is optional and can be added to capture additional conditions to change the flow of our program
*/

let numB = 1;
if (numA>0) {
	console.log('Hello')
} else if (numB>0) {
	console.log('World')
}

// another example:

city = 'Tokyo'
if (city === 'New York') {
	console.log('Welcome to New York city')
} else if (city === 'Tokyo') {
	console.log('Welcome to Tokyo')
}

/*
	else statment
		- executes a statement if all other conditions are false
*/

if (numA>0) {
	console.log('Hello')
} else if (numB===0) {
	console.log('World')
} else {
	console.log('Again')
}

// let age = parseInt(prompt("Enter your age"));
/*parseInt para maconvert yung string(sa prompt) to a number*/
/*if (age <= 18) {
	console.log('Not Allowed to Drink')
} else {
	console.log('Matanda ka na, shot na!')
}*/



/*let height = parseInt(prompt('Enter your height'))
function heightRequirement(height) {
	if (height < 150) {
	console.log('Did not passed the minimum height requirement')
	} else {
	console.log('Passed the minimum height requirement')
	}
}
heightRequirement(height)*/


let message = 'No message'
console.log(message)

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30) {
		return 'Not a typhoon yet'
	} else if (windSpeed <= 61) {
		return 'Tropical depression detected'
	} else if (windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected'
	} else if (windSpeed >= 89 && windSpeed <= 177) {
		return 'Severe tropical storm detected'
	} else {
		return 'typhoon detected'
	}
}
message=determineTyphoonIntensity(70)
console.log(message)

if (message == 'Tropical storm detected') {
	console.warn(message)
}

// Truthy and Falsy
/*
	in Js. a truthy value is a value that is considered true when encountered in a boolean context.
	
	falsy values:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/

// truthy
if (true) {
	console.log('Truthy')
}

if (1) {
	console.log('Truthy')
}

if ([]) {
	console.log('Truthy')
}

// falsy examples
if (false) {
	console.log('falsy')
}

if (0) {
	console.log('falsy')
}

if (undefined) {
	console.log('falsy')
}

// Conditional (Ternary) Operator
/*
	Ternary operator takes in three operands.
	1. condition
	2. expression to execute if the condition is true
	3. expression to execute if our condition is falsy
	
	Syntax:
	(condition) ? ifTrue_expression : ifFalse_expression

*/

// Single statement execution
let ternaryResult = (1 < 18) ? true : false
console.log("Result of Ternart Operator: " + ternaryResult)

// pwede din magprint ng string
/*let ternaryResult = (1 < 18) ? "condition is true" : "condition is false"
console.log("Result of Ternart Operator: " + ternaryResult)*/


// Multiple statement execution
/*let name;

function isOfLegalAge() {
	name = 'John'
	return 'You are of the legal age limit'
}

function isUnderAge() {
	name = 'Jane'
	return 'You are under the age limit'
}
let age = parseInt(prompt('What is your age??'))
console.log(age)
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge()
console.log('The result og ternary operator in functions: '+legalAge+ ', '+ name)*/


// Switch statement
/*
	can be used as an alternative to an if.. else statement where the data to used in the condition is of an expected input

	syntax:
		switch (expression) {
			case <value>:
				statement;
				break;

			default:
				statement;
				break;
		}
*/

// example
let day = prompt('What day of the week is it today?').toLowerCase()
console.log(day)
// toLowerCase - cinoconvert lahat into lower case
// case - sensitive sa upper and lower case

switch (day) {

	case 'monday':
		console.log('The color of the day is red.')
		break;
	case 'tuesday':
		console.log('The color of the day is orange.')
		break;
	case 'wednesday':
		console.log('The color of the day is yellow.')
		break;
	case 'thursday':
		console.log('The color of the day is green.')
		break;
	case 'friday':
		console.log('The color of the day is blue.')
		break;
	case 'saturday':
		console.log('The color of the day is indigo.')
		break;
	case 'sunday':
		console.log('The color of the day is violet.')
		break;

	default:
		console.log('Please input a valid day')
		break;
}

// Try-Catch-Finally Statement
/*
	try-catch are commonly used for error handling
*/

function showIntensityAlert (windSpeed) {
	try {
		alerat(determineTyphoonIntensity(windSpeed))
	}
	catch (error) {
		console.log(typeof error)
		console.warn(error.message)
	}
	finally {
		alert('Intensity updates will show new alert!!')
	}
	/*finally statement mag rurun kahit mag success or fail yung try-catch. okay lang din kahit walang finally statement magwowork pa din yung try-catch*/
}
showIntensityAlert(56)